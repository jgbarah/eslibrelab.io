# What's this?

This is the infrastructure needed for the web of the esLibre congress

It's based on:

* [Jekyll](https://jekyllrb.com/), as static site generator, to take advantage of GitLab features for this
* [Bootstrap](https://getbootstrap.com/) v4.0 for CSS and styling
* [Feather](https://feathericons.com/) as the set of icons used

# How is the structure?

The idea is to use `_config.yml` and `_data` files to store most of the relevant
_dynamic_ information about the congress to be
used in the website. For example, the year of the congress is noted there as:
```
year: 2019
```

So, the main `index.html` uses that value to redirect from `eslib.re` to `eslib.re/2019`

# How to contribute?

We recommend to clone the repo locally and build the site locally.

Install dependencies to run Jekyll in your computer:
```
$ bundle install
```

One required `gem`s are installed, you build and serve locally the site:
```
$ bundle exec jekyll serve
```

Visit `http://localhost:4000` to see how it looks like

You can make changes to the website code and it will be auto-re-build locally, so
you can check how they look like and how the work before committing and pushing
them to master.