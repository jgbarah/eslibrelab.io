---
layout: 2019/post
section: post
category: 2019
title: Tenemos grupo de Telegram
---

Estrenamos grupo Telegram!!!

La comunidad esLibre crece y se establece. Tras el congreso estrenamos un grupo Telegram abierto. Estáis invitados todos: participantes del pasado congreso, los que participaréis en el próximo, los antiguos amigos de los tiempos de __HispaLinux__ y los nuevos amigos, todos interesados en las tecnologías abiertas y en su adopción en la sociedad.

Os esperamos en [https://t.me/esLibre](https://t.me/esLibre).

<div style="padding-bottom: 7em;" />
