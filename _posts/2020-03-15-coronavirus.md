---
layout: 2020/post
section: post
category: 2020
title: Aviso en relación con la situación por epidemia de coronavirus
---

Debido a la situación actual debida a la epidemia de coronavirus no sabemos si las fechas de celebración previstas para esLibre 2020 pueden quedar afectadas. Por ese motivo, avisamos que vamos a esperar un tiempo prudencial para decidir, según vaya evolucionando la situación, entre las siguientes opciones:

* Mantener las fechas previstas, 5 y 6 de junio.

* Retrasar la celebración del congreso unas semanas o meses.

* Quizás (aunque por ahora es la última opción que consideramos) celebrar el congreso de forma virtual.

Por ahora, además, dejamos sin efecto el fin del plazo para recibir propuestas de contribucion (que estaba previsto para el 16 de marzo), dado que no parecen las fechas más adecuadas ni para realizar propuestas, ni para revisarlas de la forma adecuada y decidir sobre ellas. Si tienes una propuesta preparada, puedes enviarla hasta el nuevo plazo límite (cuando se comunique), pero por ahora no podemos asegurar cuándo será revisada.

Si alguna de las salas que ya han sido aceptadas quiere iniciar su periodo para recibir contribuciones, puede hacerlo. Pero si lo hace, rogamos que incluya información sobre la actual incertidumbre sobre fechas y formato del congreso.

Durante las próximas semanas informaremos de las decisiones que se vayan tomando, tanto sobre la fecha y formato de celebración del congreso, como de la nueva fecha límite de recepción de propuestas.
