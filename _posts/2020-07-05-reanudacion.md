---
layout: 2020/post
section: post
category: 2020
title: ¡Seguimos adelante! / We keep going!
---

**[Español]** Como ya sabéis, hace un par de meses tuvimos que anunciar que la celebración de <strong>es<span class="red">Libre</span> 2020</strong> se suspendía indefinidamente debido al COVID-19. Por este motivo y para garantizar la seguridad de todos los participantes y asistentes, hemos decidido que la opción más segura será hacer un evento totalmente online.

Finalmente las fechas elegidas para el evento serán los días 18 y 19 de septiembre. Dado que nuestra intención es reivindicar la importancia de la cultura libre en todas sus vertientes mediante el uso del software y el hardware libre... ¿qué mejor fecha para celebrar <strong>es<span class="red">Libre</span></strong> que el <a href="https://www.softwarefreedomday.org/" target="_blank">Software Freedom Day (Día de la Libertad de Software)</a>?

También hemos escrito a todas aquellas personas cuyas actividades/salas propuestos fueron aceptados para informarles acerca de las novedades (las listadas en <a href="https://propuestas.eslib.re/2020/" target="_blank">https://propuestas.eslib.re/2020/</a>). Nos encantaría poder mantener todas estas actividades, pero comprendemos que debido al cambio de fechas y el formato del congreso, no todas las personas pueden continuar comprometiéndose a mantener su participación en las actividades propuestas originalmente, ya que incluso algunos talleres más prácticos son difíciles para llevar a cabo de esta manera.

Por esta razón, aún no podemos confirmar si abriremos o no un nuevo período para recibir nuevas propuestas.

Dentro de poco iremos dando más información, pero en cualquier caso, si tenéis cualquier duda (o si sois participantes y no habéis recibido el correo con las nuevas noticias), podéis escribirnos a [propuestas@eslib.re](mailto:propuestas@eslib.re).

¡Nos vemos pronto!

<hr>

**[English]** As you know, a couple of months ago we had to announce that the celebration of <strong>es<span class="red">Libre</span> 2020</strong> was suspended indefinitely due to COVID-19. For this reason and in order to ensure the safety of all participants and attendees, we have decided that the safest option will be to make a totally online event.

Finally, the dates chosen for the event will be <strong>September 18th and 19th</strong>. Since our intention is to claim the importance of free culture in all its aspects through the use of software and hardware freedom… what better date to celebrate <strong>es<span class="red">Libre</span></strong> than <a href="https://www.softwarefreedomday.org/" target="_blank">Software Freedom Day</a>?

We have also written to all those people whose proposed activities/devrooms were accepted to inform them about news (those listed at <a href="https://propuestas.eslib.re/2020/" target="_blank">https://propuestas.eslib.re/2020/</a>). We would love to keep all these activities, but we understand that due to the change of dates and the format of the congress, not everyone can continue to commit to maintain their participation with the activities originally proposed, since even some more practical workshops are difficult to carry out in this way.

For this reason, we cannot yet confirm whether or not we will open a new period to receive new proposals.

We will be giving more information shortly, but in any case, if you have any questions (or if you are a participant and haven’t received the email with the new news), you can write to [propuestas@eslib.re](mailto:propuestas@eslib.re).

See you soon!
