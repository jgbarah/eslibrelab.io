---
layout: 2020/post
section: post
category: 2020
title: Esquema del congreso / Conference schema
---

**[See English version below]**

**[Español]** Acabamos de publicar la primera información detallada sobre el esquema del congreso esLibre 2020, que tendrá lugar virtualmente los días 18 y 19 de septiembre.

Vamos a usar como herramientas principales BigBlueButton y RocketChat, siguiendo el principio de que la infraestructura que utilicemos incluya tanto software libre como nos sea posible.

Puedes leer más detalles en la [página de información práctica](/2020/info-practica/)

Este esquema va a ser presentado en una sesión en BigBlueButton:

* Cuándo: Miércoles 5 de agosto, 18:00 CEST / 16:00 UTC
* Cómo: https://bbb.eslibre.urjc.es/b/jes-b17-myc

<hr>

**[English]** We have just published the first detailed information about the schema for the esLibre 2020 conference, which will be held online on September 18th and 19th.

We will be using, as main tools, BigBlueButton and RocketChat, following the principle of including as much free, open source software as we can for our infrastructure.

You can read more details in the [practical information page](/2020/info-practica/) (Spanish).

This schema is to be presented in a BigBlueButton session:

* When: Friday, August 5th, 18:00 CEST / 16:00 UTC
* How: https://bbb.eslibre.urjc.es/b/jes-b17-myc
